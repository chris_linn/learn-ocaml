# mess

+ OCaml的注释是用 `(*`和 `*)` 括起
    * 可嵌套
+ 小括号 `()` 其实就相当于 begin/end
+ 由于函数的优先级最高, 应此很多时候参数需要加上括号被优先计算
+ 由于 type inference 的副作用，OCaml 不允许任何形式的重载（包括操作符重载）。它用不同的运算符来表示“两个整数相加”（用`+`）和“两个浮点数相加”（用`+.`）。注意后者有一个点号。其他算术运算符也是这样。
+ `unit` 类型有点象C中的 `void` 类型
+ 隐式转换和显式转换哪个更好？
+ 和源于C的语言不同的是， OCaml 中的函数一般不是递归的，除非你用 `let rec` 代替 `let` 来定义递归函数。


# compiling

+ ocamlc
    + bytecode compiler
+ ocamlopt
    + native code compiler
    + faster


# simple expression

+ type
    + OCaml is a strongly typed functional language
    + result?
        * _type_ = _value_
    + Numerical
        + `1+2`
    + String
        + `"Mary"`
    + char
        + basic element of a string
        + `'a'`
    + list
        * `[ 42; 1; 55 ]`
            * int list
    + An array of strings
        * `[|"John"; "Doe"|]`
            + string array
            + `|`
+ basic operations
    + lists
        + reversing the list
            + `List.rev [1; 2; 3]`
        + first element
            + `List.hd [1; 2; 3]`
    + arrays
        + concatenate
            + `Array.append [| 1; 2 |] [| 3; 4; 5 |]`
        + To get the element number i of an array
            + `Array.get [| 42; 51; 32 |] 2`
            + `[| 42; 51; 32 |].(2)`
    + strings
        + `String.make 10 'x'`
        + `"Mary" ^ " and " ^ "John"`
        + `String.length "abcdefghijklmnopqstuvwxyz"`
        + `String.lowercase "MARY"`
        + `String.concat "/" [""; "usr"; "local"; "bin"]`
            + __don't know__ why no `|` here
        + int_of_string "546"
+ tuple
    + `(42, "John", true)`
    + `fst (42, "John")`
    + `snd (42, "John")`



# Imperative Programming

+ Variables
    + `let x = 6 * 7`
    + `x`
    + cannot change the value associated with a name after its definition
        + __error__: `x <- x + 1`
        + so?
            + special trick using function `ref`
                * `:=` 操作符用来给引用赋值， `!` 操作符用来取出引用的值（解引用）。
                + let x = ref 42
                + x := 100 / 4
                + x
                + can also access the value contained in the reference using the operator `!`
                    + `let y = !x + 1`
+ Sequences and Printing
    + __hang up as try ocaml is not working good__
+ For loops
    + 
        ```
        let xl = ref []

        for i = 1 to 10 do
            xl := i :: !xl;
        done;

        !xl
        
        List.rev !xl

        for i = 10 downto 1 do
            xl := i :: !xl;
        done;
        ```
+ Computing Conditions
    + "aaaaaa" < "bbb"
    + "3" <= "22"
        + `false`
    + 22 >= 3
    + To test equality
        + `=`
        + OCaml does not allow you to compare values with different typesa
            + 
                ```
                "1" = string_of_int 1
                ```
    + If then else
        + 
            ```
            let z = if a < 100 then begin
                print_string "return at least 100 !";
                print_newline ();
            100
            end else a
            ```
    + While loops
        + 
            ```
                while !x > 20 do
                    print_int !x; print_newline ();
                    x := !x - 2
                done
            ```

+ function
    + Defining a one-argument function
        + `let divide x y = (x / y, x mod y)`
    + partial application
        + 
            ```
                let plus x y = x + y
                let incr = plus 1
            '''
    + Anonymous functions
        + `(fun x -> x + 1) 42`
        + bound an anonymous function to an identifier
            + still anonymous but with id
                + hmmm
            +
                ```
                    let incr = fun x -> x + 1
                    incr 42
                ```

