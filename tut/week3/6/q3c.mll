{
let get = Lexing.lexeme
let put = Printf.printf
}

let start   = '/' '*'
let end     = '*'+ '/'
let normalc = [^ '"' '/' '*']
let ndbquot = [^ '"']
let string  = '"' ndbquot* '"'
let middle  = '/' | '*'* (normalc | string)
let comment = start middle* end

rule tokens = parse
  | comment { put "<<<%s>>>" (get lexbuf); tokens lexbuf }
  | eof     { () }
  | _       { put "%s" (get lexbuf); tokens lexbuf }

{
  let _ = tokens (Lexing.from_channel (open_in Sys.argv.(1))) ;;
}
