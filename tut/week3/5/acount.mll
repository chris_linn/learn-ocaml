(*
lantana
*)

(*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*)

(*
mine
*)


{
let get = Lexing.lexeme
let put = Printf.printf
let cnt_and = ref 0
let cnt_an = ref 0
let cnt_a = ref 0
}


rule tokens = parse
| "and" { put "%s" (get lexbuf); cnt_and := !cnt_and+1; tokens lexbuf }
| "an" { put "%s" (get lexbuf); cnt_an := !cnt_an+1; tokens lexbuf }
| 'a' { put "%s" (get lexbuf); cnt_a := !cnt_a+1; tokens lexbuf }
| eof { () }
| _ { put "%s" (get lexbuf); tokens lexbuf }

{
let _ = 
        let content = Lexing.from_channel (open_in Sys.argv.(1))
    in tokens content; put "\ncnt_and=%d cnt_an=%d cnt_a=%d\n" !cnt_and !cnt_an !cnt_a;;
}

(*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*)

(*
sample

{
type word = A | AN | AND | EOF | OTHER
let put = Printf.printf
}

(*seems the sequence does not matter!*)
(*use of pattern matching!*)
(*pattern matching can be used for specific data or types!*)


rule tokens = parse
                | "a" { A }
                | "an" { AN }
                | "and" { AND }
                | eof { EOF }
                | _ { OTHER }
{
let _ =
        let rec count input a_cnt an_cnt and_cnt 
            = match (tokens input) with
                |  A -> count input (a_cnt+1) an_cnt and_cnt
                | AN -> count input a_cnt (an_cnt+1) and_cnt
                | AND -> count input a_cnt an_cnt (and_cnt+1)
                | EOF -> put "a: %i, an: %i, and: %i\n" a_cnt an_cnt and_cnt
                | OTHER -> count input a_cnt an_cnt and_cnt
        in
          count (Lexing.from_channel (open_in Sys.argv.(1))) 0 0 0 ;;
}
*)




(*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*)

(*
try self-defined lexbuf (i.e. input)

{
type word = A | AN | AND | EOF | OTHER
let put = Printf.printf
}

(*seems the sequence does not matter!*)
(*use of pattern matching!*)
(*pattern matching can be used for specific data or types!*)


rule tokens = parse
                | "a" { A }
                | "an" { AN }
                | "and" { AND }
                | eof { EOF }
                | _ { OTHER }
{
let _ =
        let rec count input a_cnt an_cnt and_cnt 
            = match (tokens input) with
                |  A -> count input (a_cnt+1) an_cnt and_cnt
                | AN -> count input a_cnt (an_cnt+1) and_cnt
                | AND -> count input a_cnt an_cnt (and_cnt+1)
                | EOF -> put "a: %i, an: %i, and: %i\n" a_cnt an_cnt and_cnt
                | OTHER -> put "%s\n" (Lexing.lexeme input); count input a_cnt an_cnt and_cnt
            in
          count (Lexing.from_channel (open_in Sys.argv.(1))) 0 0 0 ;;
}

*)


