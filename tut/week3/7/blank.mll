{
let get = Lexing.lexeme
let put = Printf.printf
let is_leading = ref true
let f is_leading = match (!is_leading) with
                    | true -> ()
                    | false -> put " "
}

let white = [' ''\t']+
let ret = '\n'

rule tokens = parse
  | white { f is_leading; tokens lexbuf }
  | ret { put "(eol)\n"; is_leading := true; tokens lexbuf }
  | eof     { put "(eof)" }
  | _       { is_leading := false; put "%s" (get lexbuf); tokens lexbuf }

{
let _ = tokens (Lexing.from_channel (open_in Sys.argv.(1))) ;;
}
