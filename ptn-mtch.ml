
(*pattern matching can be used for specific data or types!*)


type sb =
        | S of string 
        | B of string;;

let f x = match x with
            | (S y) -> "s:"^y 
            | (B y) -> "b:"^y;; 

f (S "sa");;